# Sistema de transações bancária

Sistema resolve dois casos de uso

1. Consulta transações bancária
2. Grava transações bancária de um cliente

![consulta](imagens/atividade-consulta.png)
> Figura 1. Diagrama UML Atividade representando consulta de transações

![Grava](imagens/atividade-grava.png)
> Figura 2. Diagrama UML Atividade representando gravar transações bancária

O sistema foi modelado a partir do diagrama de classes

![classe](imagens/dominio.png)
> Figura 3. Diagrama UML de classes representando modelo de domínio

Uma visão dos componentes do sistema

![classe](imagens/componente.png)
> Figura 4. Diagrama UML de componentes representando o design do sistema
