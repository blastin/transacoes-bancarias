package br.projeto.transacoes.adaptadores.newpersistence.conta;

import java.util.Optional;

public interface ContaBancariaRepositorio {

    Optional<ContaBancariaEntity> encontrarPorAgenciaEContaCorrente(final String agencia, final String contaCorrente);

}
