package br.projeto.transacoes.adaptadores.newpersistence.transacao;

import br.projeto.transacoes.adaptadores.newpersistence.conta.ContaBancariaRepositorio;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoGateway;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public final class TransactionNewDatabaseFacade implements TransacaoGateway {

    public TransactionNewDatabaseFacade
            (
                    final TransacaoRepositorio transacaoRepositorio,
                    final ContaBancariaRepositorio contaBancariaRepositorio
            ) {
        this.transacaoRepositorio = transacaoRepositorio;
        this.contaBancariaRepositorio = contaBancariaRepositorio;
    }

    private final TransacaoRepositorio transacaoRepositorio;

    private final ContaBancariaRepositorio contaBancariaRepositorio;

    @Override
    public void salvar(final Collection<Transacao> transactions) {

        if (transactions.isEmpty()) return;

        final Transacao transaction = transactions.iterator().next();

        final ContaBancaria contaBancaria = transaction.obterContaBancaria();

        contaBancariaRepositorio
                .encontrarPorAgenciaEContaCorrente(contaBancaria.getAgencia(), contaBancaria.getContaCorrente())
                .ifPresent
                        (
                                contaBancariaEntity ->
                                        transacaoRepositorio.salvarTodos
                                                (
                                                        transactions
                                                                .stream()
                                                                .map(t -> new TransacaoEntity(t, contaBancariaEntity))
                                                                .collect(Collectors.toUnmodifiableSet())
                                                )
                        );

    }

    @Override
    public Set<Transacao> listar(final ContaBancaria contaBancaria) {

        return transacaoRepositorio
                .transacoesPorAgenciaEContaCorrente(contaBancaria.getAgencia(), contaBancaria.getContaCorrente())
                .stream()
                .map(TransacaoEntity::toTransacao)
                .collect(Collectors.toUnmodifiableSet());

    }

}
