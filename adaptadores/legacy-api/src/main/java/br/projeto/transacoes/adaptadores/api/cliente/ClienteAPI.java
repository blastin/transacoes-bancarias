package br.projeto.transacoes.adaptadores.api.cliente;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente-repository", url = "${cliente.url}", decode404 = true)
public interface ClienteAPI {

	@GetMapping(value = "clientes/{cpf}", consumes = MediaType.APPLICATION_JSON_VALUE)
	Optional<ClienteModel> recuperarPorCPF(@PathVariable final String cpf);

}
