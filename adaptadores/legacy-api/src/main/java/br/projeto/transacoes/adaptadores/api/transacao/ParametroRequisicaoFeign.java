package br.projeto.transacoes.adaptadores.api.transacao;

import br.projeto.transacoes.domain.conta.ContaBancaria;

@SuppressWarnings("unused")
public final class ParametroRequisicaoFeign {

    ParametroRequisicaoFeign(final ContaBancaria contaBancaria) {
        this.contaBancaria = contaBancaria;
    }

    private final ContaBancaria contaBancaria;

    public String getAgencia() {
        return contaBancaria.getAgencia();
    }

    public String getConta() {
        return contaBancaria.getContaCorrente();
    }

}
