package br.projeto.transacoes.adaptadores.api.transacao;

import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoQuery;

import java.util.Set;
import java.util.stream.Collectors;

public final class TransacaoQueryAPIAdapter implements TransacaoQuery {

    public TransacaoQueryAPIAdapter(final TransactionAPI transactionAPI) {
        this.transactionAPI = transactionAPI;
    }

    private final TransactionAPI transactionAPI;

    @Override
    public Set<Transacao> listar(final ContaBancaria contaBancaria) {
        return transactionAPI
                .recuperarPorAgenciaContaCorrente
                        (
                                new ParametroRequisicaoFeign(contaBancaria)
                        )
                .stream()
                .map(transacaoModel -> transacaoModel.toTransacao(contaBancaria))
                .collect(Collectors.toUnmodifiableSet());
    }
}
