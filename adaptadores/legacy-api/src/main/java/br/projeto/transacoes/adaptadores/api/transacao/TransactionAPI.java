package br.projeto.transacoes.adaptadores.api.transacao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

@FeignClient(name = "transaction-repository", url = "${transaction.url}", decode404 = true)
public interface TransactionAPI {

    @GetMapping(value = "transacoes", consumes = MediaType.APPLICATION_JSON_VALUE)
    Collection<TransacaoModel> recuperarPorAgenciaContaCorrente(@SpringQueryMap final ParametroRequisicaoFeign parametros);

}
