package br.projeto.transacoes.adaptadores.web.configuration;

import java.util.Optional;
import java.util.Set;

class JCollections {

    static <E> Optional<Set<E>> toOptional(final Set<E> collection) {
        return Optional.ofNullable(collection.isEmpty() ? null : collection);
    }

    private JCollections() {
    }

}
