package br.projeto.transacoes.adaptadores.web.configuration;

import br.project.knin.domain.DomainQuery;
import br.projeto.transacoes.adaptadores.api.cliente.ClienteAPI;
import br.projeto.transacoes.adaptadores.api.cliente.ClienteQueryAPIAdapter;
import br.projeto.transacoes.adaptadores.api.transacao.TransacaoQueryAPIAdapter;
import br.projeto.transacoes.adaptadores.api.transacao.TransactionAPI;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.cliente.ClienteQuery;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;
import br.projeto.transacoes.domain.transacao.TransacaoQuery;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@EnableFeignClients(basePackages = "br.projeto.transacoes.adaptadores.api")
class LegacyConfiguration {

    @Bean(name = "legacy-cliente-api")
    DomainQuery<String, Cliente> clienteDomainQueryAPI(final ClienteAPI clienteAPI) {

        final ClienteQuery clienteQuery = new ClienteQueryAPIAdapter(clienteAPI);

        return clienteQuery::recuperarPorCpf;

    }

    @Bean(name = "legacy-transaction-api")
    DomainQuery<ContaBancaria, Set<Transacao>> transactionDomainQueryAPI(final TransactionAPI transactionAPI) {

        final TransacaoQuery transacaoQuery = new TransacaoQueryAPIAdapter(transactionAPI);

        return contaBancaria -> JCollections.toOptional(transacaoQuery.listar(contaBancaria));

    }

}
