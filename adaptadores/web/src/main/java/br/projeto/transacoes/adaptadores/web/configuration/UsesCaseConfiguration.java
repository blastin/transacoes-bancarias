package br.projeto.transacoes.adaptadores.web.configuration;

import br.projeto.transacoes.adaptadores.web.adapter.TabelaStatusWebAdapter;
import br.projeto.transacoes.aplicacao.consulta.CasoDeUsoApresentarTransacoesBancaria;
import br.projeto.transacoes.aplicacao.grava.CasoDeUsoGravarCliente;
import br.projeto.transacoes.aplicacao.resposta.TabelaStatus;
import br.projeto.transacoes.domain.cliente.ClienteCommand;
import br.projeto.transacoes.domain.cliente.ClienteQuery;
import br.projeto.transacoes.domain.transacao.TransacaoCommand;
import br.projeto.transacoes.domain.transacao.TransacaoQuery;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class UsesCaseConfiguration {

    @Bean
    TabelaStatus TabelaStatus() {
        return new TabelaStatusWebAdapter();
    }

    @Bean
    CasoDeUsoApresentarTransacoesBancaria casoDeUsoConsultaTransacoesBancaria
            (
                    final TabelaStatus tabelaStatus,
                    final ClienteQuery clienteQuery,
                    final TransacaoQuery transacaoQuery
            ) {

        return CasoDeUsoApresentarTransacoesBancaria
                .create
                        (
                                clienteQuery,
                                transacaoQuery,
                                tabelaStatus
                        );

    }

    @Bean
    CasoDeUsoGravarCliente casoDeUsoGravarCliente
            (
                    final ClienteCommand clienteCommand,
                    final TransacaoCommand transacaoCommand
            ) {

        return CasoDeUsoGravarCliente.create(clienteCommand, transacaoCommand);

    }
}
