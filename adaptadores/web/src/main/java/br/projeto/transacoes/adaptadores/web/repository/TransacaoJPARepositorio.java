package br.projeto.transacoes.adaptadores.web.repository;

import br.projeto.transacoes.adaptadores.newpersistence.transacao.TransacaoEntity;
import br.projeto.transacoes.adaptadores.newpersistence.transacao.TransacaoRepositorio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

public interface TransacaoJPARepositorio extends JpaRepository<TransacaoEntity, UUID> {

    final class TransacaoRepositorioAdapter implements TransacaoRepositorio {

        public TransacaoRepositorioAdapter(final TransacaoJPARepositorio transacaoJPARepositorio) {
            this.transacaoJPARepositorio = transacaoJPARepositorio;
        }

        private final TransacaoJPARepositorio transacaoJPARepositorio;

        @Override
        public Collection<TransacaoEntity> transacoesPorAgenciaEContaCorrente
                (
                        final String agencia,
                        final String contaCorrente
                ) {

            return transacaoJPARepositorio.
                    findAllByContaBancariaEntity_AgenciaAndContaBancariaEntity_ContaCorrente(agencia, contaCorrente);

        }

        @Override
        public void salvarTodos(final Set<TransacaoEntity> transactions) {
            transacaoJPARepositorio.saveAll(transactions);
        }

    }

    Collection<TransacaoEntity> findAllByContaBancariaEntity_AgenciaAndContaBancariaEntity_ContaCorrente
            (final String agencia, final String contaCorrente);

}
