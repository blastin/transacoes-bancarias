package br.projeto.transacoes.adaptadores.web.resources;

import br.projeto.transacoes.adaptadores.web.model.ErroModel;
import br.projeto.transacoes.adaptadores.web.model.RequisicaoConsultaModel;
import br.projeto.transacoes.adaptadores.web.model.RequisicaoGravaModel;
import br.projeto.transacoes.adaptadores.web.model.TransactionResponseModel;
import br.projeto.transacoes.aplicacao.consulta.CasoDeUsoApresentarTransacoesBancaria;
import br.projeto.transacoes.aplicacao.consulta.RequisicaoConsulta;
import br.projeto.transacoes.aplicacao.consulta.RespostaConsulta;
import br.projeto.transacoes.aplicacao.grava.CasoDeUsoGravarCliente;
import br.projeto.transacoes.aplicacao.resposta.Erro;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@RestController
class Controller {

    private Controller
            (
                    final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria,
                    final CasoDeUsoGravarCliente casoDeUsoGravarCliente
            ) {
        this.casoDeUsoApresentarTransacoesBancaria = casoDeUsoApresentarTransacoesBancaria;
        this.casoDeUsoGravarCliente = casoDeUsoGravarCliente;
    }

    private final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria;

    private final CasoDeUsoGravarCliente casoDeUsoGravarCliente;

    @GetMapping("clientes/{cpf}/transactions")
    private ResponseEntity<TransactionResponseModel> consultarTransacoes
            (
                    @PathVariable(value = "cpf", required = false) final String cpf,
                    @RequestParam(value = "agencia", required = false) final String agencia,
                    @RequestParam(value = "conta", required = false) final String contaCorrente,
                    @RequestParam(value = "data_inicio", required = false) final String initialDate,
                    @RequestParam(value = "data_fim", required = false) final String finalDate
            ) {

        final RequisicaoConsulta requisicaoConsulta =
                RequisicaoConsultaModel
                        .builder()
                        .comCpf(cpf)
                        .daAgencia(agencia)
                        .comContaCorrente(contaCorrente)
                        .paraPeriodo(initialDate, finalDate)
                        .build();

        final RespostaConsulta respostaConsulta = casoDeUsoApresentarTransacoesBancaria
                .consultarTransacoes(requisicaoConsulta);

        return ResponseEntity
                .status(respostaConsulta.obterStatus())
                .body(TransactionResponseModel.of(respostaConsulta));

    }

    @PostMapping("clientes")
    private ResponseEntity<Collection<ErroModel>> salvarClientes
            (
                    @Valid @RequestBody final RequisicaoGravaModel requisicaoGravaModel
            ) {

        final AtomicReference<Set<Erro>> atomicReference = new AtomicReference<>(Set.of());

        casoDeUsoGravarCliente.processar(requisicaoGravaModel, atomicReference::set);

        final Set<ErroModel> erroModelSet =
                atomicReference
                        .get()
                        .stream()
                        .map(ErroModel::new)
                        .collect(Collectors.toUnmodifiableSet());

        if (!erroModelSet.isEmpty()) return ResponseEntity.badRequest().body(erroModelSet);

        return ResponseEntity.status(HttpStatus.CREATED).build();

    }
}
