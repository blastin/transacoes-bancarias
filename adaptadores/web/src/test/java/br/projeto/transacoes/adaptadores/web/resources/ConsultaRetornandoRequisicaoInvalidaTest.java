package br.projeto.transacoes.adaptadores.web.resources;

import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Intervalo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ConsultaRetornandoRequisicaoInvalidaTest {

    private static final TipoErro INVALIDO = TipoErro.INVALIDO;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void quandoRequisicaoComCpfInvalidoRetornarRespostaErro() throws Exception {

        final LinkedMultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();

        final Cliente cliente = Cliente.mock();

        final ContaBancaria contaBancaria = cliente.obterContasBancaria().iterator().next();

        valueMap.add("agencia", contaBancaria.getAgencia());
        valueMap.add("conta", contaBancaria.getContaCorrente());
        valueMap.add("data_inicio", "01/01/2020");
        valueMap.add("data_fim", "01/01/2021");

        mockMvc
                .perform
                        (
                                get("/clientes/{cpf}/transactions", 1)
                                        .params(valueMap)
                        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros").isArray())
                .andExpect(jsonPath("$.erros", hasSize(1)))
                .andExpect(jsonPath("$.erros[0].valor", equalTo("1")))
                .andExpect(jsonPath("$.erros[0].campo", equalTo(TabelaCampo.CPF.toString())))
                .andExpect(jsonPath("$.erros[0].mensagem", equalTo(INVALIDO.toString())))
                .andExpect(jsonPath("$.erros[0].codigo", equalTo(INVALIDO.toInt())));

    }

    @Test
    void quandoRequisicaoComAgenciaInvalidaRetornarRespostaErro() throws Exception {

        final LinkedMultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();

        final Cliente cliente = Cliente.mock();

        final ContaBancaria contaBancaria = cliente.obterContasBancaria().iterator().next();

        valueMap.add("agencia", "");
        valueMap.add("conta", contaBancaria.getContaCorrente());
        valueMap.add("data_inicio", "01/01/2020");
        valueMap.add("data_fim", "01/01/2021");

        mockMvc
                .perform
                        (
                                get("/clientes/{cpf}/transactions", cliente.obterCpf())
                                        .params(valueMap)
                        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros").isArray())
                .andExpect(jsonPath("$.erros", hasSize(1)))
                .andExpect(jsonPath("$.erros[0].valor", equalTo("")))
                .andExpect(jsonPath("$.erros[0].campo", equalTo(TabelaCampo.AGENCIA.toString())))
                .andExpect(jsonPath("$.erros[0].mensagem", equalTo(INVALIDO.toString())))
                .andExpect(jsonPath("$.erros[0].codigo", equalTo(INVALIDO.toInt())));

    }

    @Test
    void quandoRequisicaoComContaCorrenteInvalidaRetornarRespostaErro() throws Exception {

        final LinkedMultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();

        final Cliente cliente = Cliente.mock();

        final ContaBancaria contaBancaria = cliente.obterContasBancaria().iterator().next();

        valueMap.add("agencia", contaBancaria.getAgencia());
        valueMap.add("conta", "");
        valueMap.add("data_inicio", "01/01/2020");
        valueMap.add("data_fim", "01/01/2021");

        mockMvc
                .perform
                        (
                                get("/clientes/{cpf}/transactions", cliente.obterCpf())
                                        .params(valueMap)
                        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros").isArray())
                .andExpect(jsonPath("$.erros", hasSize(1)))
                .andExpect(jsonPath("$.erros[0].valor", equalTo("")))
                .andExpect(jsonPath("$.erros[0].campo", equalTo(TabelaCampo.CONTA_CORRENTE.toString())))
                .andExpect(jsonPath("$.erros[0].mensagem", equalTo(INVALIDO.toString())))
                .andExpect(jsonPath("$.erros[0].codigo", equalTo(INVALIDO.toInt())));

    }

    @Test
    void quandoRequisicaoComDataInicioInvalidaRetornarRespostaErro() throws Exception {

        final LinkedMultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();

        final Cliente cliente = Cliente.mock();

        final ContaBancaria contaBancaria = cliente.obterContasBancaria().iterator().next();

        valueMap.add("agencia", contaBancaria.getAgencia());
        valueMap.add("conta", contaBancaria.getContaCorrente());

        final String dataInicio = "01/01/202X";

        valueMap.add("data_inicio", dataInicio);

        final String dataFim = "01/01/2021";

        valueMap.add("data_fim", dataFim);

        mockMvc
                .perform
                        (
                                get("/clientes/{cpf}/transactions", cliente.obterCpf())
                                        .params(valueMap)
                        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros").isArray())
                .andExpect(jsonPath("$.erros", hasSize(2)))
                .andExpect(jsonPath("$.erros[*].valor", containsInAnyOrder(dataInicio, Intervalo.formatarIntervalo(dataInicio, dataFim))))
                .andExpect(jsonPath("$.erros[*].campo", containsInAnyOrder(TabelaCampo.INTERVALO.toString(), TabelaCampo.DATA_INICIO.toString())))
                .andExpect(jsonPath("$.erros[*].mensagem", containsInAnyOrder(INVALIDO.toString(), INVALIDO.toString())))
                .andExpect(jsonPath("$.erros[*].codigo", containsInAnyOrder(INVALIDO.toInt(), INVALIDO.toInt())));

    }

    @Test
    void quandoRequisicaoComDataFimInvalidaRetornarRespostaErro() throws Exception {

        final LinkedMultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();

        final Cliente cliente = Cliente.mock();

        final ContaBancaria contaBancaria = cliente.obterContasBancaria().iterator().next();

        valueMap.add("agencia", contaBancaria.getAgencia());
        valueMap.add("conta", contaBancaria.getContaCorrente());

        final String dataInicio = "01/01/2021";

        valueMap.add("data_inicio", dataInicio);

        final String dataFim = "01/01/202X";

        valueMap.add("data_fim", dataFim);

        mockMvc
                .perform
                        (
                                get("/clientes/{cpf}/transactions", cliente.obterCpf())
                                        .params(valueMap)
                        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros").isArray())
                .andExpect(jsonPath("$.erros", hasSize(2)))
                .andExpect(jsonPath("$.erros[*].valor", containsInAnyOrder(dataFim, Intervalo.formatarIntervalo(dataInicio, dataFim))))
                .andExpect(jsonPath("$.erros[*].campo", containsInAnyOrder(TabelaCampo.INTERVALO.toString(), TabelaCampo.DATA_FIM.toString())))
                .andExpect(jsonPath("$.erros[*].mensagem", containsInAnyOrder(INVALIDO.toString(), INVALIDO.toString())))
                .andExpect(jsonPath("$.erros[*].codigo", containsInAnyOrder(INVALIDO.toInt(), INVALIDO.toInt())));

    }

}