package br.projeto.transacoes.adaptadores.web.resources;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;

import java.nio.file.Files;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("test")
class GravaRetornandoCreatedTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void quandoEncaminharRequisicaoParaGravarDadosClienteDeveRetornarCriadoTest() throws Exception {

        final String json = new String
                (
                        Files
                                .readAllBytes
                                        (
                                                ResourceUtils
                                                        .getFile("classpath:__files/json/gravar-transacoes-cliente.json")
                                                        .toPath()
                                        )
                );

        mockMvc
                .perform
                        (
                                post("/clientes")
                                        .content
                                                (json)
                                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        )
                .andDo(print())
                .andExpect(status().isCreated());

    }
}
