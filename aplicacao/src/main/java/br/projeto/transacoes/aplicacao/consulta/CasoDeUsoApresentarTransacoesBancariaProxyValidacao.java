package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TabelaStatus;
import br.projeto.transacoes.aplicacao.validate.ValidateBuilder;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Intervalo;

import java.util.Set;

final class CasoDeUsoApresentarTransacoesBancariaProxyValidacao implements CasoDeUsoApresentarTransacoesBancaria {

    private static Set<Erro> casoExistamErrosRetorneUmaColecao(final RequisicaoConsulta requisicaoConsulta) {

        return ValidateBuilder
                .init()
                .ifInvalid
                        (
                                requisicaoConsulta::obterCpf,
                                requisicaoConsulta::obterCpf,
                                Cliente::cpfValido,
                                TabelaCampo.CPF
                        )
                .ifInvalid
                        (
                                requisicaoConsulta::obterDataInicio,
                                requisicaoConsulta::obterDataInicio,
                                Intervalo::dataValida,
                                TabelaCampo.DATA_INICIO
                        )
                .ifInvalid
                        (
                                requisicaoConsulta::obterDataFim,
                                requisicaoConsulta::obterDataFim,
                                Intervalo::dataValida,
                                TabelaCampo.DATA_FIM
                        )
                .ifInvalid
                        (
                                () -> requisicaoConsulta,
                                requisicaoConsulta::obterDataInicioFim,
                                req -> Intervalo.datasEmIntervaloValido
                                        (
                                                req.obterDataInicio(),
                                                req.obterDataFim()
                                        ),
                                TabelaCampo.INTERVALO
                        )
                .ifInvalid
                        (
                                requisicaoConsulta::obterAgencia,
                                requisicaoConsulta::obterAgencia,
                                s -> !ContaBancaria.agenciaInvalida(s),
                                TabelaCampo.AGENCIA
                        )
                .ifInvalid
                        (
                                requisicaoConsulta::obterContaCorrente,
                                requisicaoConsulta::obterContaCorrente,
                                s -> !ContaBancaria.contaCorrenteInvalida(s),
                                TabelaCampo.CONTA_CORRENTE
                        )
                .build();

    }

    CasoDeUsoApresentarTransacoesBancariaProxyValidacao
            (
                    final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria,
                    final TabelaStatus tabelaStatus
            ) {
        this.casoDeUsoApresentarTransacoesBancaria = casoDeUsoApresentarTransacoesBancaria;
        this.tabelaStatus = tabelaStatus;
    }

    private final CasoDeUsoApresentarTransacoesBancaria casoDeUsoApresentarTransacoesBancaria;

    private final TabelaStatus tabelaStatus;

    @Override
    public RespostaConsulta consultarTransacoes(final RequisicaoConsulta requisicaoConsulta) {

        final Set<Erro> erros = casoExistamErrosRetorneUmaColecao(requisicaoConsulta);

        if (!erros.isEmpty())
            return RespostaConsultaBuilder.valueOf(erros, tabelaStatus.statusRequestInvalid());

        return casoDeUsoApresentarTransacoesBancaria.consultarTransacoes(requisicaoConsulta);

    }

}
