package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.domain.transacao.CategoriaTransacao;

import java.util.Map;
import java.util.Set;

public interface RespostaConsulta {

    class SkeletalRespostaConsulta implements RespostaConsulta {

        private SkeletalRespostaConsulta
                (
                        final int status,
                        final Set<Erro> errors,
                        final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionsMap
                ) {
            this.status = status;
            this.errors = errors;
            this.transactionsMap = transactionsMap;
        }

        private final int status;

        private final Set<Erro> errors;

        private final Map<CategoriaTransacao, Set<RespostaTransacao>> transactionsMap;

        @Override
        public int obterStatus() {
            return status;
        }

        @Override
        public Set<Erro> obterErros() {
            return errors;
        }

        @Override
        public Map<CategoriaTransacao, Set<RespostaTransacao>> obterTransacoes() {
            return transactionsMap;
        }

    }

    static RespostaConsulta nullable(final int status) {
        return new SkeletalRespostaConsulta(status, Set.of(), Map.of());
    }

    int obterStatus();

    Set<Erro> obterErros();

    Map<CategoriaTransacao, Set<RespostaTransacao>> obterTransacoes();

}
