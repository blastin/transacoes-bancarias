package br.projeto.transacoes.aplicacao.consulta;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface RespostaTransacao {

    LocalDateTime obterDataHoraTransacao();

    String obterDescricao();

    BigDecimal obterValor();

}
