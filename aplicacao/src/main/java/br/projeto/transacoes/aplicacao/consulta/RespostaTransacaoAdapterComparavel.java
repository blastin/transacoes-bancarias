package br.projeto.transacoes.aplicacao.consulta;

import br.projeto.transacoes.domain.transacao.Transacao;

import java.math.BigDecimal;
import java.time.LocalDateTime;

final class RespostaTransacaoAdapterComparavel implements RespostaTransacao, Comparable<RespostaTransacaoAdapterComparavel> {

    /**
     * Static factory method necessário, pois preciso do tipo da interface
     *
     * @param transacao instância de transação não nula
     * @return uma nova instância do tipo da interface RespostaTransacao
     */
    static RespostaTransacao valueOf(final Transacao transacao) {
        return new RespostaTransacaoAdapterComparavel(transacao);
    }

    private RespostaTransacaoAdapterComparavel(final Transacao transacao) {
        this.transacao = transacao;
    }

    private final Transacao transacao;

    @Override
    public LocalDateTime obterDataHoraTransacao() {
        return transacao.obterDataHoraEfetivacao();
    }

    @Override
    public String obterDescricao() {
        return transacao.obterDescricao();
    }

    @Override
    public BigDecimal obterValor() {
        return transacao.obterValor();
    }

    @Override
    public int compareTo(final RespostaTransacaoAdapterComparavel respostaTransacaoAdapterComparavel) {
        return -transacao.compareTo(respostaTransacaoAdapterComparavel.transacao);
    }

}
