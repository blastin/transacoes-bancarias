package br.projeto.transacoes.aplicacao.grava;

import br.project.knin.activity.Channel;
import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.domain.cliente.ClienteCommand;
import br.projeto.transacoes.domain.transacao.TransacaoCommand;

import java.util.Set;

@FunctionalInterface
public interface CasoDeUsoGravarCliente {

    /**
     * Com foco na diminuição de classes do tipo factory, create é um static method factory util para facilitar a construção
     * de instâncias do caso de uso gravar cliente.
     *
     * @param clienteCommand   instância para interface funcional cliente Command.
     * @param transacaoCommand instância para interface funcional transacao Command.
     * @return uma nova instância de caso de uso.
     */
    static CasoDeUsoGravarCliente create(final ClienteCommand clienteCommand, final TransacaoCommand transacaoCommand) {
        return new CasoDeUsoGravarClienteValidacaoProxy(new CasoDeUsoGravarClienteImpl(clienteCommand, transacaoCommand));
    }

    void processar(final RequisicaoClienteComTransacoes requisicaoClienteComTransacoes, final Channel<Set<Erro>> channelError);

}
