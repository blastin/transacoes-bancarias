package br.projeto.transacoes.aplicacao.grava;

import br.project.knin.activity.Channel;
import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.validate.ValidateBuilder;
import br.projeto.transacoes.domain.cliente.Cliente;
import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;

final class CasoDeUsoGravarClienteValidacaoProxy implements CasoDeUsoGravarCliente {

    private static void validarCliente
            (
                    final ValidateBuilder validateBuilder,
                    final RequisicaoClienteComTransacoes requisicaoClienteComTransacoes
            ) {

        validateBuilder
                .ifInvalid
                        (
                                requisicaoClienteComTransacoes::obterCpf,
                                requisicaoClienteComTransacoes::obterCpf,
                                Cliente::cpfValido,
                                TabelaCampo.CPF
                        );

    }

    private static void validarContasBancaria
            (
                    final ValidateBuilder validateBuilder,
                    final Collection<RequisicaoContaBancaria> contasBancaria
            ) {

        contasBancaria
                .forEach
                        (
                                contaBancaria ->
                                        validateBuilder
                                                .ifInvalid
                                                        (
                                                                contaBancaria::obterAgencia,
                                                                contaBancaria::obterAgencia,
                                                                s -> !ContaBancaria.agenciaInvalida(s),
                                                                TabelaCampo.AGENCIA
                                                        )
                                                .ifInvalid
                                                        (
                                                                contaBancaria::obterContaCorrente,
                                                                contaBancaria::obterContaCorrente,
                                                                s -> !ContaBancaria.contaCorrenteInvalida(s),
                                                                TabelaCampo.CONTA_CORRENTE
                                                        )
                        );
    }

    private static void validarTransacoes
            (
                    final ValidateBuilder validateBuilder,
                    final Collection<RequisicaoContaBancaria> contasBancaria
            ) {

        contasBancaria
                .stream()
                .flatMap(contaBancaria -> contaBancaria.obterRequisicaoTransacoes().stream())
                .forEach
                        (
                                requisicaoTransacao ->
                                        validateBuilder
                                                .ifInvalid
                                                        (
                                                                requisicaoTransacao::obterValor,
                                                                () -> requisicaoTransacao.obterValor().toString(),
                                                                Transacao::valorValido,
                                                                TabelaCampo.VALOR
                                                        )
                        );
    }

    CasoDeUsoGravarClienteValidacaoProxy(final CasoDeUsoGravarCliente casoDeUsoGravarCliente) {
        this.casoDeUsoGravarCliente = casoDeUsoGravarCliente;
    }

    private final CasoDeUsoGravarCliente casoDeUsoGravarCliente;

    @Override
    public void processar(final RequisicaoClienteComTransacoes requisicaoClienteComTransacoes, final Channel<Set<Erro>> channelError) {

        Objects.requireNonNull(requisicaoClienteComTransacoes, "requisição não pode ser nula");

        Objects.requireNonNull(channelError, "canal de erros não pode ser nulo");

        final ValidateBuilder validateBuilder = ValidateBuilder.init();

        final Collection<RequisicaoContaBancaria> contasBancaria = requisicaoClienteComTransacoes.obterContasBancaria();

        validarCliente(validateBuilder, requisicaoClienteComTransacoes);

        validarContasBancaria(validateBuilder, contasBancaria);

        validarTransacoes(validateBuilder, contasBancaria);

        final Set<Erro> erros = validateBuilder.build();

        if (!erros.isEmpty()) channelError.commit(erros);

        else casoDeUsoGravarCliente.processar(requisicaoClienteComTransacoes, channelError);

    }

}
