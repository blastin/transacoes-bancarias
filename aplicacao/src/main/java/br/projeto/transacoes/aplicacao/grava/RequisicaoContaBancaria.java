package br.projeto.transacoes.aplicacao.grava;

import br.projeto.transacoes.domain.conta.ContaBancaria;
import br.projeto.transacoes.domain.transacao.Transacao;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Stream;

public interface RequisicaoContaBancaria {

    class RequisicaoContaBancariaSkeletal implements RequisicaoContaBancaria {

        RequisicaoContaBancariaSkeletal(final ContaBancaria contaBancaria) {
            this.contaBancaria = contaBancaria;
        }

        private final ContaBancaria contaBancaria;

        @Override
        public String obterAgencia() {
            return contaBancaria.getAgencia();
        }

        @Override
        public String obterContaCorrente() {
            return contaBancaria.getContaCorrente();
        }

        @Override
        public Collection<RequisicaoTransacao> obterRequisicaoTransacoes() {
            return Set.of(RequisicaoTransacao.mock(Transacao.mock(contaBancaria)));
        }
    }

    static RequisicaoContaBancaria mock(final ContaBancaria contaBancaria) {
        return new RequisicaoContaBancariaSkeletal(contaBancaria);
    }

    String obterAgencia();

    String obterContaCorrente();

    Collection<RequisicaoTransacao> obterRequisicaoTransacoes();

    default ContaBancaria contaBancaria() {
        return
                ContaBancaria
                        .builder()
                        .comContaCorrente(obterContaCorrente())
                        .paraAgencia(obterAgencia())
                        .build();
    }

    default Stream<Transacao> transacoes() {
        return obterRequisicaoTransacoes().stream().map(this::transacao);
    }

    private Transacao transacao(final RequisicaoTransacao requisicaoTransacao) {
        return requisicaoTransacao.transacao(this);
    }

}
