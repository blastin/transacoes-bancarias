package br.projeto.transacoes.aplicacao.validate;

import br.projeto.transacoes.aplicacao.resposta.Erro;
import br.projeto.transacoes.aplicacao.resposta.TabelaCampo;
import br.projeto.transacoes.aplicacao.resposta.TipoErro;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class ValidateBuilder {

    public static ValidateBuilder init() {
        return new ValidateBuilder();
    }

    private static <T> Predicate<T> not(final Predicate<T> predicate) {
        return t -> !predicate.test(t);
    }

    private ValidateBuilder() {
        erros = new LinkedHashSet<>();
    }

    private final Set<Erro> erros;

    public <T> ValidateBuilder ifInvalid
            (
                    final Supplier<T> predicateValue,
                    final Supplier<String> valueProduced,
                    final Predicate<T> predicate,
                    final TabelaCampo tabelaCampo
            ) {

        final T value = predicateValue.get();

        if (not(predicate).test(value)) {
            erros.add
                    (
                            Erro
                                    .builder()
                                    .comValor(valueProduced.get())
                                    .campo(tabelaCampo)
                                    .tipoErro(TipoErro.INVALIDO)
                                    .build()
                    );
        }

        return this;

    }

    public Set<Erro> build() {
        return erros;
    }

}
