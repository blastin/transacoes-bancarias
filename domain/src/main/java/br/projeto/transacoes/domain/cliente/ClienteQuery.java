package br.projeto.transacoes.domain.cliente;

import java.util.Optional;

@FunctionalInterface
public interface ClienteQuery {

    /**
     * @param cliente instância de cliente
     * @return Retorna uma instância de ClienteQuery para realizar testes
     */
    static ClienteQuery mock(final Cliente cliente) {
        return cpf -> Optional.ofNullable(cliente).filter(c -> c.identificadoPeloCpf(cpf));
    }

    Optional<Cliente> recuperarPorCpf(final String cpf);

}
