package br.projeto.transacoes.domain.data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Dates {

    private static final DateTimeFormatter PADRAO_DATA = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static LocalDate parse(final String data) {
        return parse(data, PADRAO_DATA);
    }

    public static LocalDate parse(final String data, final DateTimeFormatter dateTimeFormatter) {
        return LocalDate.parse(data, dateTimeFormatter);
    }

    public static String format(final LocalDate data) {
        return data.format(PADRAO_DATA);
    }

    private Dates() {
    }

}
