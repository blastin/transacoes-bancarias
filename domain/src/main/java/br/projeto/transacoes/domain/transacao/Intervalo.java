package br.projeto.transacoes.domain.transacao;

import br.projeto.transacoes.domain.data.Dates;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public final class Intervalo {

    /**
     * @param data data é uma cadeia de caracteres cujo padrão deve ser : dia/mes/ano
     * @return caso a data seja valida retornará true
     */
    public static boolean dataValida(final String data) {

        if (data == null) return false;

        try {

            Dates.parse(data);

            return true;

        } catch (DateTimeParseException e) {

            return false;

        }

    }

    public static boolean datasEmIntervaloValido(final String inicio, final String fim) {

        try {

            final LocalDate inicioDate = Dates.parse(inicio);

            final LocalDate fimDate = Dates.parse(fim);

            return validarIntervalo(inicioDate, fimDate);

        } catch (DateTimeParseException e) {
            return false;
        }

    }

    private static boolean validarIntervalo(final LocalDate inicioDate, final LocalDate fimDate) {
        return fimDate.compareTo(inicioDate) >= 0;
    }

    /**
     * Instância de intervalo mock.
     * Data inicio é igual a date
     * Data fim é date + 1 dia
     *
     * @param date referencia de local date
     * @return instância de intervalo para testes
     */
    public static Intervalo mock(final LocalDate date) {
        return new Intervalo(date, date.plusDays(1));
    }

    public static Intervalo between(final String inicio, final String fim) {

        if (!dataValida(inicio)) {
            throw new IllegalArgumentException("data inicio invalida");
        }

        if (!dataValida(fim)) {
            throw new IllegalArgumentException("data inicio invalida");
        }

        return between(Dates.parse(inicio), Dates.parse(fim));

    }

    public static String formatarIntervalo(final String dataInicio, final String dataFim) {
        return String.format("%s - %s", dataInicio, dataFim);
    }

    /**
     * @param inicio data inicio
     * @param fim    data fim
     * @return uma nova instância de intervalo
     * @throws IllegalArgumentException será lançada caso inicio seja posterior a fim
     */
    public static Intervalo between(final LocalDate inicio, final LocalDate fim) {

        if (!validarIntervalo(inicio, fim)) throw new IllegalArgumentException("intervalo inválido");

        return new Intervalo(inicio, fim);

    }

    private Intervalo(final LocalDate inicio, final LocalDate fim) {
        this.inicio = inicio;
        this.fim = fim;
    }

    private final LocalDate inicio;

    private final LocalDate fim;

    boolean dentro(final LocalDate data) {

        final int compararInicio = inicio.compareTo(data);

        final int compararFim = fim.compareTo(data);

        return compararInicio <= 0 && compararFim >= 0;

    }

    @Override
    public String toString() {
        return formatarIntervalo(inicio.toString(), fim.toString());
    }
}
