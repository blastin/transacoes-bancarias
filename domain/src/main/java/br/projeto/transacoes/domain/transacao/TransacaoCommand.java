package br.projeto.transacoes.domain.transacao;

import java.util.Collection;

@FunctionalInterface
public interface TransacaoCommand {

    void salvar(final Collection<Transacao> transacoes);

}
