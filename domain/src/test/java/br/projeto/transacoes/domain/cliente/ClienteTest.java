package br.projeto.transacoes.domain.cliente;

import br.projeto.transacoes.domain.conta.ContaBancaria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ClienteTest {

    @Test
    void deveRetornarCpfValido() {

        Assertions.assertTrue(Cliente.cpfValido(Cliente.mock().obterCpf()));

    }

    @Test
    void deveRetornarCpfInvalido() {

        Assertions.assertThrows
                (
                        IllegalArgumentException.class,
                        () -> Cliente
                                .builder(Cliente.mock())
                                .comCpf("38374756")
                );

    }

    @Test
    void deveRetornarCpfInvalidoParaCpfNull() {

        Assertions.assertFalse(Cliente.cpfValido(null));

    }

    @Test
    void deveRetornarCpfSemMascara() {

        final Cliente cliente = Cliente
                .builder(Cliente.mock())
                .comCpf("603.236.930-75")
                .build();

        Assertions.assertTrue(cliente.identificadoPeloCpf("60323693075"));

    }

    @Test
    void deveRetornarClienteNaoHabilitado() {

        final Cliente cliente = Cliente.mock();

        Assertions.assertFalse(cliente.habilitado());

        Assertions.assertEquals(SituacaoCliente.OUTRO, cliente.obterSituacao());

    }

    @Test
    void naoDeveRetornarUmaContaBancaria() {

        final Cliente cliente = Cliente.mock();

        Assertions.assertTrue(cliente.contaBancaria("7898", "78945-8").isEmpty());

    }

    @Test
    void deveRetornarUmaContaBancaria() {

        final Cliente cliente = Cliente.mock();

        final ContaBancaria contaBancaria = ContaBancaria.mock();

        Assertions.assertTrue
                (
                        cliente
                                .contaBancaria(contaBancaria.getAgencia(), contaBancaria.getContaCorrente())
                                .isPresent()
                );

    }

}