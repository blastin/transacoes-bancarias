package br.projeto.transacoes.domain.conta;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ContaBancariaTest {

    @Test
    void deveRetornarAgenciaInvalida() {
        Assertions.assertTrue(ContaBancaria.agenciaInvalida(""));
        Assertions.assertTrue(ContaBancaria.agenciaInvalida("123q"));
    }

    @Test
    void deveRetornarAgenciaInvalidaPoisEhNula() {
        Assertions.assertTrue(ContaBancaria.agenciaInvalida(null));
    }

    @Test
    void deveRetornarAgenciaValida() {
        Assertions.assertFalse(ContaBancaria.agenciaInvalida(ContaBancaria.mock().getAgencia()));
    }

    @Test
    void deveRetornarContaCorrenteInvalida() {
        Assertions.assertTrue(ContaBancaria.contaCorrenteInvalida(""));
        Assertions.assertTrue(ContaBancaria.contaCorrenteInvalida("1w-3"));
    }

    @Test
    void deveRetornarContaCorrenteInvalidaPoisEhNula() {
        Assertions.assertTrue(ContaBancaria.contaCorrenteInvalida(null));
    }

    @Test
    void deveRetornarContaCorrenteValida() {
        Assertions.assertFalse(ContaBancaria.contaCorrenteInvalida(ContaBancaria.mock().getContaCorrente()));
    }

    @Test
    void agenciaInvalidaDeVeLevantarExcecaoIllegalArgument() {
        Assertions
                .assertThrows
                        (
                                IllegalArgumentException.class,
                                () -> ContaBancaria.builder(ContaBancaria.mock()).paraAgencia("")
                        );
    }

    @Test
    void contaCorrenteInvalidaDeVeLevantarExcecaoIllegalArgument() {
        Assertions
                .assertThrows
                        (
                                IllegalArgumentException.class,
                                () -> ContaBancaria.builder(ContaBancaria.mock()).comContaCorrente("")
                        );
    }

    @Test
    void deveConstruirUmaContaCorrente() {
        Assertions.assertDoesNotThrow(() -> ContaBancaria.builder(ContaBancaria.mock()).paraAgencia("4567").comContaCorrente("12495-6").build());
    }

    @Test
    void contasBancariaComAgenciaDiferente() {

        final ContaBancaria contaBancaria = ContaBancaria.mock();

        Assertions.assertNotEquals(contaBancaria, ContaBancaria.builder(contaBancaria).paraAgencia("4567").build());

    }

    @Test
    void contasBancariaComContaCorrenteDiferente() {

        final ContaBancaria contaBancaria = ContaBancaria.mock();

        Assertions.assertNotEquals(contaBancaria, ContaBancaria.builder(contaBancaria).comContaCorrente("12495-6").build());

    }

    @Test
    void contasBancariaIguais() {

        final ContaBancaria contaBancaria = ContaBancaria.mock();

        Assertions.assertEquals
                (
                        contaBancaria,
                        ContaBancaria.builder(contaBancaria).build()
                );

    }

    @Test
    void contasBancariaDeMesmaInstancia() {

        final ContaBancaria contaBancaria = ContaBancaria.mock();

        Assertions.assertEquals(contaBancaria, contaBancaria);

    }

    @Test
    void comparacaoComNull() {
        Assertions.assertNotEquals(ContaBancaria.mock(), null);
    }

    @Test
    void comparacaoComOutroTipoDeObjeto() {
        Assertions.assertNotEquals(ContaBancaria.mock(), new Object());
    }

}
