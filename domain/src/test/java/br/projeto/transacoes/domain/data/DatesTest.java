package br.projeto.transacoes.domain.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

class DatesTest {

    @Test
    void deveRetornarLocalDateComPadraoData() {

        final LocalDate localDate = Dates.parse("27/09/2020");

        Assertions.assertEquals(27, localDate.getDayOfMonth());

        Assertions.assertEquals(9, localDate.getMonthValue());

        Assertions.assertEquals(2020, localDate.getYear());

    }

    @Test
    void deveLancarExcecaoPorSerNula() {

        Assertions.assertThrows(NullPointerException.class, () -> Dates.parse(null));

    }

    @Test
    void deveLancarExcecaoPoisParseNaoFoiRealizado() {

        Assertions.assertThrows(DateTimeParseException.class, () -> Dates.parse("null"));

    }

}