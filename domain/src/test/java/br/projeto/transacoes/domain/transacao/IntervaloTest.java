package br.projeto.transacoes.domain.transacao;

import br.projeto.transacoes.domain.data.Dates;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class IntervaloTest {

    @Test
    void deveRetornarDataValida() {

        Assertions.assertTrue(Intervalo.dataValida("27/09/2020"));

    }


    @Test
    void deveRetornarDataInvalidaQuandoParametroNulo() {

        Assertions.assertFalse(Intervalo.dataValida(null));

    }

    @Test
    void deveLancarExcecaoPorDataInicioInvalida() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Intervalo.between("", ""));
    }


    @Test
    void deveLancarExcecaoPorDataFimInvalida() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> Intervalo.between(Dates.format(LocalDate.now()), ""));
    }

    @Test
    void deveRetornarDataInvalidaQuandoformatoNaoEhReconhecida() {

        Assertions.assertFalse(Intervalo.dataValida("20200827"));

    }

    @Test
    void quandoAnteriorDataInicioDeveRetornarFalso() {

        final LocalDate agora = LocalDate.now();

        final Intervalo intervalo = Intervalo.mock(agora);

        Assertions.assertFalse(intervalo.dentro(agora.minusDays(1)));

    }

    @Test
    void quandoPosteriorADataFimDeveRetornarFalso() {

        final LocalDate agora = LocalDate.now();

        final Intervalo intervalo = Intervalo.between(agora, agora.plusDays(1));

        Assertions.assertFalse(intervalo.dentro(agora.plusDays(2)));

    }

    @Test
    void quandoIgualDataInicioDeveRetornarVerdadeiro() {

        final LocalDate agora = LocalDate.now();

        final Intervalo intervalo = Intervalo.between(agora, agora.plusDays(2));

        Assertions.assertTrue(intervalo.dentro(agora));

    }

    @Test
    void quandoIgualDataFimDeveRetornarVerdadeiro() {

        final LocalDate amanha = LocalDate.now().plusDays(1);

        final Intervalo intervalo = Intervalo.between(amanha.minusDays(1), amanha);

        Assertions.assertTrue(intervalo.dentro(amanha));

    }

    @Test
    void quandoDataFinalEhIgualDataInicial() {

        final LocalDate now = LocalDate.now();

        final String dateFormatted = Dates.format(now);

        Assertions
                .assertTrue
                        (
                                Intervalo
                                        .datasEmIntervaloValido(dateFormatted, dateFormatted)
                        );

    }

    @Test
    void quandoDataFinalEhPosteriorDataInicial() {

        final LocalDate now = LocalDate.now();

        Assertions
                .assertTrue
                        (
                                Intervalo
                                        .datasEmIntervaloValido(Dates.format(now), Dates.format(now.plusDays(1)))
                        );

    }

    @Test
    void quandoDataFinalEhAnteriorDataInicial() {

        final LocalDate now = LocalDate.now();

        Assertions
                .assertFalse
                        (
                                Intervalo
                                        .datasEmIntervaloValido(Dates.format(now), Dates.format(now.minusDays(1)))
                        );

    }

    @Test
    void quandoDataInicialInvalida() {

        final LocalDate now = LocalDate.now();

        Assertions
                .assertFalse
                        (
                                Intervalo
                                        .datasEmIntervaloValido("Dates.format(now)", Dates.format(now.plusDays(1)))
                        );

    }

    @Test
    void quandoDataFimInvalida() {

        final LocalDate now = LocalDate.now();

        Assertions
                .assertFalse
                        (
                                Intervalo
                                        .datasEmIntervaloValido(Dates.format(now), "Dates.format(now.plusDays(1))")
                        );

    }

    @Test
    void quandoIntervaloConstruidoPorCadeiaDeCaracteres() {

        final LocalDate now = LocalDate.now();

        final String inicio = Dates.format(now);

        final String fim = Dates.format(now.plusMonths(1));

        final Intervalo intervalo = Intervalo.between(inicio, fim);

        Assertions.assertTrue(intervalo.dentro(now.plusWeeks(1)));

    }

    @Test
    void quandoDataInicioPosteriorDataFimUmaExecaoDeveSerLevantada() {

        final LocalDate now = LocalDate.now();

        Assertions.assertThrows(IllegalArgumentException.class, () -> Intervalo.between(now.plusDays(1), now));

    }
}