package br.projeto.transacoes.domain.transacao;

import br.projeto.transacoes.domain.conta.ContaBancaria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.Set;

class TransacaoQueryTest {

    @Test
    void shouldReturnCollectionOfTransactions() {

        final Transacao transacao = Transacao.mock();

        final TransacaoQuery transacaoQuery = TransacaoQuery.mock(transacao);

        final Set<Transacao> transactions = transacaoQuery.listar(ContaBancaria.mock());

        Assertions.assertEquals(7, transactions.size());

        final Optional<Transacao> optional =
                transactions
                        .stream()
                        .filter(t -> t.obterDataHoraEfetivacao().equals(transacao.obterDataHoraEfetivacao()))
                        .findFirst();

        Assertions.assertTrue(optional.isPresent());

    }
}